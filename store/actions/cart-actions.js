export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_ORDER = 'ADD_ORDER';

export const addToCart = (productToAdd) => {
    return {
        type: ADD_TO_CART,
        productToAdd: productToAdd
    };
}

export const removeFromCart = (productId) => {
    return {type: REMOVE_FROM_CART, pid: productId};
};

export const addOrder = () => {
    return {type: ADD_ORDER};
};