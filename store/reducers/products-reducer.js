import PRODUCTS from '../../data/dummy-data';
import { DELETE_PRODUCT, UPDATE_PRODUCT, CREATE_PRODUCT } from '../actions/products-action';
import Product from '../../models/product';


const initialState = {
  availableProducts: PRODUCTS,
  userProducts: PRODUCTS.filter((product) => product.ownerId === 'u1')
};

const ProductsReducer = (state = initialState, action) => {
  switch (action.type) {

    case CREATE_PRODUCT:
      const newProduct = new Product(
        new Date().toString(),
        'u1', // user dummy ID
        action.productData.title,
        action.productData.imageUrl,
        action.productData.description,
        action.productData.price
      );
      return {
        ...state,
        availableProducts: state.availableProducts.concat(newProduct), // JOIN the arrays
        userProducts: state.userProducts.concat(newProduct)
      };

    case UPDATE_PRODUCT:
      const productIndex = state.userProducts.findIndex(
        prod => prod.id === action.pid
      );
      const updatedProduct = new Product(
        action.pid,
        state.userProducts[productIndex].ownerId,
        action.productData.title,
        action.productData.imageUrl,
        action.productData.description,
        action.productData.price
      );
      const updatedUserProducts = [...state.userProducts]; // taking current products list
      updatedUserProducts[productIndex] = updatedProduct; // updatating the specific prod that the user has created.

      const availableProductIndex = state.availableProducts.findIndex(
        prod => prod.id === action.pid
      );

      const updatedAvailableProducts = [...state.availableProducts];
      updatedAvailableProducts[availableProductIndex] = updatedProduct; // updating the products to show in main page "products overview scren"
      return {
        ...state,
        availableProducts: updatedAvailableProducts,
        userProducts: updatedUserProducts
      };


    case DELETE_PRODUCT:
      return {
        ...state,
        userProducts: state.userProducts.filter(
          product => product.id !== action.pid
        ),
        availableProducts: state.availableProducts.filter(
          product => product.id !== action.pid
        )
      };
    default:
      return state;
  }
};

export default ProductsReducer;