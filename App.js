import 'react-native-gesture-handler'; // must be ALWAYS FIRST
import React, {useState} from 'react';
import { Platform } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Font from 'expo-font';
import { AppLoading} from 'expo';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import {createStore, combineReducers} from 'redux';
import { Provider }  from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import ProductsOverviewScreen from './screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from './screens/shop/ProductDetailScreen';
import EditProductScreen from './screens/user/EditProductScreen';

import CartScreen from './screens/shop/CartScreen';
import OrdersScreen from './screens/shop/OrdersScreen';
import ProductsReducer from './store/reducers/products-reducer';
import cartReducer from './store/reducers/cart-reducer';
import ordersReducer from './store/reducers/orders-reducer';
import Colors from './constants/Colors';
import UserProductsScreen from './screens/user/UserProductsScreen';

const rootReducer = combineReducers({
  productsState: ProductsReducer,
  cartState: cartReducer,
  ordersState: ordersReducer
});


const store = createStore(rootReducer, composeWithDevTools(
));

const fetchFonts=() => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'opens-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

const Stack = createStackNavigator();

function stackScreens({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('tabPress', e => {
      e.preventDefault();
      let tabPressed = e.target.split("-")[0].toUpperCase();
      // I need to do this, because just setting the page as STACK navigitation , those page will have header
      switch (tabPressed) {
        case 'HOME':
          navigation.navigate('ProductsOverviewScreen');
        break;

        case 'ORDERS':
          navigation.navigate('OrdersScreen');
        break;

        case 'ADMIN':
          navigation.navigate('UserProductsScreen');
        break;
        
        default:
          navigation.navigate('ProductsOverviewScreen');
        break;
      }
    });

    return unsubscribe;
  }, [navigation]);
  return (
    <Provider store={store}>
        <Stack.Navigator>
          <Stack.Screen name="ProductsOverviewScreen" component={ProductsOverviewScreen} options={{ title: 'All Products' }}/>
          <Stack.Screen name="ProductDetailScreen" component={ProductDetailScreen} options={{ title: 'Product Details' }}/>
          <Stack.Screen name="CartScreen" component={CartScreen} options={{ title: 'Cart' }}/>
          <Stack.Screen name="OrdersScreen" component={OrdersScreen} options={{ title: 'Your Orders' }}/>
          <Stack.Screen name="UserProductsScreen" component={UserProductsScreen} options={{ title: 'Your Products' }}/>
          <Stack.Screen name="EditProductScreen" component={EditProductScreen} options={{ title: 'Edit Products' }}/>
        </Stack.Navigator>
    </Provider>
  );
}

const Tab = createBottomTabNavigator();

export const App = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if(!fontsLoaded){
    return <AppLoading startAsync={fetchFonts} onFinish={()=> setFontsLoaded(true)}/>
  }
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName="CategoriesScreen" // main screen
        screenOptions={({ route }) => ({
          headerStyle: {
            backgroundColor: Colors.headerBackGroundColor,
          },
          headerTintColor: Colors.headerTintColor,
          headerTitleStyle: {
            fontWeight: Colors.headerfontWeight,
          },
        })}
      >
        <Tab.Screen name="Home" component={stackScreens}  options={{
          tabBarLabel: 'Home',
          tabBarIcon:() => (
            <Ionicons name='ios-home' size={16} color='black' />
          ),
        }} />
        <Tab.Screen name="Orders" component={stackScreens} options={{
          tabBarLabel: 'Orders',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-cash' : 'ios-cash'} size={16} color='black' />
          )
        }}/>
        <Tab.Screen name="Admin" component={stackScreens} options={{
          tabBarLabel: 'Admin',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-settings' : 'ios-settings'} size={16} color='black' />
          )
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
} 

export default App;