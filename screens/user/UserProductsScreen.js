import React from 'react';
import { FlatList, Button, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import ProductItemFlatList from '../../components/shop/ProductItemFlatList';
import Colors from '../../constants/Colors';
import * as productsActions from '../../store/actions/products-action';

const UserProductsScreen = ({ route, navigation }) => {
    const userProducts = useSelector(state => state.productsState.userProducts);
    const dispatch = useDispatch();


    const editProductHandler = id => {
        navigation.navigate('EditProductScreen', { productId: id });
    };

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you really want to delete this item?', [
            { text: 'No', style: 'default' },
            {
                text: 'Yes',
                style: 'destructive',
                onPress: () => {
                    dispatch(productsActions.deleteProduct(id));
                }
            }
        ]);
    };
    return (
        <FlatList
            data={userProducts}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <ProductItemFlatList
                    image={itemData.item.imageUrl}
                    title={itemData.item.title}
                    price={itemData.item.price}
                    onSelect={() => { editProductHandler(itemData.item.id); }}
                >
                    {/* These buttons will be handle using 'props.children' in  ProductItemFlatList*/}
                    <Button color={Colors.primary} title="Edit" onPress={() => { editProductHandler(itemData.item.id); }} />
                    <Button color={Colors.primary} title="Create" onPress={() => { editProductHandler(null); }} />
                    <Button
                        color={Colors.primary}
                        title="Delete"
                        onPress={() => {
                            deleteHandler(itemData.item.id)
                        }}
                    />
                </ProductItemFlatList>
            )}
        />
    );
};

export default UserProductsScreen;
