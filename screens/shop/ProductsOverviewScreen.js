import React from 'react';
import { StyleSheet, FlatList, Platform, Button } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { Badge } from 'react-native-elements';

import ProductItemFlatList from '../../components/shop/ProductItemFlatList';
import * as cartActions from '../../store/actions/cart-actions';
import Colors from '../../constants/Colors';
import CustomHeaderButton from '../../components/UI-utils/CustomHeaderButtons';

export const ProductsOverviewScreen = (props) => {
    const {navigation} = props;
    const dispatch = useDispatch();
    const products = useSelector ((state) => state.productsState.availableProducts);

    const cartTotalProducts = useSelector(state => {
      const cartItems = [];
      for (const key in state.cartState.items) {
        cartItems.push({
              productId: key,
        });
      }
      return cartItems;
    });

    const selectItemHandler = (id, title) => {
      props.navigation.navigate('ProductDetailScreen', {
        productId: id,
        productTitle: title
      });
    };

    React.useLayoutEffect(() => {
        navigation.setOptions({
          headerRight: () => ( 
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Badge
                  status="success"
                  containerStyle={{ position: 'absolute', top: -13, right: 0 }}
                  value= {cartTotalProducts && cartTotalProducts.length}
                />
                <Item title='Orders' iconName={Platform.OS === 'android' ? 'md-cash' : 'ios-cash'} onPress={()=> {navigation.navigate('OrdersScreen')}}/>
                <Item title='Cart' iconName={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'} onPress={()=> {navigation.navigate('CartScreen')}}/>
             </HeaderButtons>
          )
        });
      }, [products,cartTotalProducts])
  
    return (
        <FlatList
            data={products}
            keyExtractor={item => item.id}
            renderItem={itemData => (
            // attention: the params for the Item render function  MUST BE SPLITED ONE BY ONE  
            <ProductItemFlatList
                image={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={() => {
                  selectItemHandler(itemData.item.id, itemData.item.title);
                }}
              >
              {/* These buttons will be handle using 'props.children' in  ProductItemFlatList*/}
              <Button
                color={Colors.primary}
                title="View Details"
                onPress={() => {
                  selectItemHandler(itemData.item.id, itemData.item.title);
                }}
              />
              <Button
                color={Colors.primary}
                title="To Cart"
                onPress={() => {
                  dispatch(cartActions.addToCart(itemData.item));
                }}
              />
            </ProductItemFlatList>
        )}
      />
    )
}

const styles = StyleSheet.create({

});
export default ProductsOverviewScreen;