import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    ScrollView,
    View,
    Text,
    Image,
    Button,
    StyleSheet
} from 'react-native';

import Colors from '../../constants/Colors';

import * as cartActions from '../../store/actions/cart-actions';

export const ProductDetailScreen = ({ route, navigation }) => {
    const { productId, productTitle } = route.params;
    const selectedProduct = useSelector(state => state.productsState.availableProducts.find((prod) => prod.id === productId));
    const dispatch = useDispatch();

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: productTitle,
            //   headerRight: () => ( 
            //     <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            //         <Item title='Favorites' iconName={favMealsSaved ? 'ios-star' : 'ios-star-outline'} onPress={()=> { dispatchAction()} }/>
            //      </HeaderButtons>
            //   )

        });
    }, [route.params])

    return (
        <ScrollView>
            <Image style={styles.image} source={{ uri: selectedProduct.imageUrl }} />
            <View style={styles.actions}>
                <Button color={Colors.primary} title="Add to Cart" onPress={() => { dispatch(cartActions.addToCart(selectedProduct)) }} />
            </View>
            <Text style={styles.price}>${selectedProduct.price.toFixed(2)}</Text>
            <Text style={styles.description}>{selectedProduct.description}</Text>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 300
    },
    actions: {
        marginVertical: 10,
        alignItems: 'center'
    },
    price: {
        fontSize: 20,
        color: Colors.price,
        textAlign: 'center',
        marginVertical: 20
    },
    description: {
        fontSize: 14,
        textAlign: 'center',
        marginHorizontal: 20,
        fontFamily: 'opens-sans-bold'
    }
});
export default ProductDetailScreen;