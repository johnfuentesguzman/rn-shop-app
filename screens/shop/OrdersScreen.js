import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList
} from 'react-native';
import { useSelector } from 'react-redux';

import OrderItemFlatList from '../../components/shop/OrderItemFlatList';

const OrdersScreen = props => {
    const orders = useSelector(state => state.ordersState.orders);

    if (orders.length === 0) {
        return (
            <View style={styles.noOrdersContainer}>
                <Text style={styles.noOrdersText}>No Orders to Show</Text>
            </View>
        );
    }
    return (
        <FlatList
            data={orders}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <OrderItemFlatList
                  amount={itemData.item.totalAmount.toFixed(2)}
                  date={itemData.item.readableDate}
                  items={itemData.item.items}
                />
            )}
        />
    );
};

const styles = StyleSheet.create({
    noOrdersContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    noOrdersText: {
        fontFamily: 'opens-sans-bold',
        fontSize: 16
    }
});
export default OrdersScreen;
