class CartItem  {
    constructor(productQuantity, productPrice, productTitle, priceSummatory){
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
        this.productTitle = productTitle;
        this.priceSummatory = priceSummatory
    }
}

export default CartItem;